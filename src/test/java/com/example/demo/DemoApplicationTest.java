package com.example.demo;

import com.example.demo.entity.User;
import com.example.demo.repository.MessageRepo;
import com.example.demo.repository.UserRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DemoApplicationTest {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private MessageRepo messageRepo;

    @Test
    public void createUser() {
        User user = new User("New user", "123");
        userRepo.save(user);
        assertEquals(user, userRepo.findByUsername("New user"));
    }

    @Test
    public void editUser() {
        userRepo.save(new User("User 1", "123"));
        User user = userRepo.findByUsername("User 1");
        String oldName = user.getUsername();
        user.setUsername("Alexey");
        userRepo.save(user);
        assertNotEquals(oldName, userRepo.findById(user.getId()).getUsername());
    }

    @Test
    public void deleteUser() {
        userRepo.save(new User("User_for_deleted", "123"));
        userRepo.findAll().forEach(user -> System.out.println(user.getUsername()));
        long count = userRepo.count();
        userRepo.delete(userRepo.findByUsername("User_for_deleted"));
        assertEquals(count - 1, userRepo.count());
    }

}